package getpayment;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        IEmployee[] employees = new IEmployee[n];
        for(int i = 0; i < n; i ++){
            System.out.println("Enter employee type (1 for FullTime, 2 for PartTime): ");
            int employeeType = sc.nextInt();
            if (employeeType == 1) {
                System.out.println("Name: ");
                String name = sc.next();
                System.out.println("PaymentPerHour: ");
                int paymentPerHour = sc.nextInt();
                employees[i] = new FullTimeEmployee(name, paymentPerHour);
            } else if (employeeType == 2) {
                System.out.println("Name: ");
                String name = sc.next();
                System.out.println("PaymentPerHour: ");
                int paymentPerHour = sc.nextInt();
                System.out.println("WorkingHours: ");
                int workingHours = sc.nextInt();
                employees[i] = new PartTimeEmployee(name, paymentPerHour, workingHours);
            } else {
                System.out.println("Invalid employee type.");
                i--; // Repeat the loop iteration for valid input.
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println("Name: " + employees[i].getName());
            System.out.println("Salary per day: " + employees[i].calculateSalary());
        }
    }
}