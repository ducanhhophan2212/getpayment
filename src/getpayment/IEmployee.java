package getpayment;

public interface IEmployee{
    int calculateSalary();
    String getName();
}